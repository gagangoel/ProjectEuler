#include <iostream>
#include "utils.h"
using namespace std;

int main() {
	int count=0;
	int num=11;
	int sum=0;
	while(true) {
		if(count==11)
			break;
		if(!is_prime(num)) {
			num++;
			continue;
		}
		vector<int> digits = num_to_vector(num);
		bool is_truncate_prime=true;
		for(int i=1; i<digits.size(); i++) {
			vector<int> left_to_right(digits.size()-i);
			vector<int> right_to_left(digits.size()-i);
			copy(digits.begin()+i, digits.end(), left_to_right.begin());
			copy(digits.begin(), digits.end()-i, right_to_left.begin());
			string str1="", str2="";
			for(auto j:left_to_right)
				str1+=to_string(j);
			for(auto k:right_to_left)
				str2+=to_string(k);
			if((!is_prime(stoi(str1)))||(!is_prime(stoi(str2)))) {
				is_truncate_prime=false;
				break;
			}
		}
		if(is_truncate_prime) {
			sum+=num;
			cout<<num<<endl;
			count++;
		}
		num++;
	}
	cout<<"Sum of the 11 truncatable prime numbers is: "<<sum<<endl;
}
