#include <iostream>
#include <unordered_set>
#include "utils.h"

using namespace std;

int main() {

  vector<unordered_set<int>> my_set = {{1, 2}, {1, 3}, {1, 4}};
  cout<<my_set.size()<<endl;
  cout<<my_set[0].size()<<endl;
  vector<vector<unordered_set<int>>> my_set_comb = combinations_fast(my_set, 2);
  cout<<my_set_comb.size()<<endl;
  cout<<my_set_comb[0].size()<<endl;
  for(int i=0; i<my_set_comb.size(); i++) {
    for(int j=0; j<my_set_comb[i].size(); j++) {
      cout<<"{";
      for(auto iter=my_set_comb[i][j].begin(); iter!=my_set_comb[i][j].end(); iter++)
        cout<<(*iter)<<",";
      cout<<"}";
    }
    cout<<endl;
  }
  vector<int> input = {1, 2, 3, 4};
  vector<vector<int>> comb_reg = combinations_fast(input, 2);
  vector<vector<int>> comb_custom = combinations_fast(input, 2, true);
  vector<vector<int>> comb_custom_template = combinations_fast<int>(input, 2, true);
  cout<<"Regular implementation: "<<endl;
  for(const auto& i : comb_reg) {
    for(const auto& j : i)
      cout<<j<<" ";
    cout<<endl;
  }
  cout<<"Custom implementation: "<<endl;
  for(const auto& i : comb_custom) {
    for(const auto& j : i)
      cout<<j<<" ";
    cout<<endl;
  }
  cout<<"Custom template implementation: "<<endl;
  for(const auto& i : comb_custom_template) {
    for(const auto& j : i)
      cout<<j<<" ";
    cout<<endl;
  }
}
