/* Solution to this problem is not programmatic, it is rather based on Euclid's formula.
If m>n, a=m^2 - n^2, b=2mn, c=m^2 + n^2
*/

#include <iostream>
#include <cmath>
using namespace std;

int main() {
	int sum = 1000;
	int a=1, b=2, c=3;
	while(true) {
		cout<<"a: "<<a<<" b: "<<b<<" c: "<<c<<endl;
		if((pow(a, 2)+pow(b, 2))<pow(c, 2))
			if(((pow(a+1, 2)+pow(b, 2))-pow(c, 2)<=0) && ((pow(a, 2)+pow(b+1, 2))-pow(c, 2)<=0))
				if((pow(a+1, 2)+pow(b, 2)) < (pow(a, 2)+pow(b+1, 2)))
					a++;
				else
					b++;
			else if(((pow(a+1, 2)+pow(b, 2))-pow(c, 2)<=0) && ((pow(a, 2)+pow(b+1, 2))-pow(c, 2)>0))
				a++;
			else
				b++;
		else if((pow(a, 2)+pow(b, 2))>pow(c, 2))
			c++;
		else {
			cout<<"Pythagorean triplet! Sum is: "<<(a+b+c)<<endl;
			if((a+b+c)==sum)
				break;
			else
				a++;
		}
	}
	cout<<"a: "<<a<<" b: "<<b<<" c: "<<c<<endl;
}
