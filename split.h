#ifndef __SPLIT_H_INCLUDED__
#define __SPLIT_H_INCLUDED__

#include <vector>

std::vector<std::string> split(const std::string &, char);
void split(const std::string &, char, std::vector<std::string>&);

#endif
