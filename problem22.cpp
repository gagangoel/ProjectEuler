#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <unordered_map>
using namespace std;

void preprocessing() {
	ifstream fin("p022_names.txt");
	vector<string> names;
	string str;
	while(getline(fin, str, ',')) {
		names.push_back(str.substr(1, str.length()-2));
	}
	cout<<"Number of names: "<<names.size()<<endl;
	fin.close();
	ofstream fout("p022_names_split.txt");
	for(int i=0; i<names.size(); i++)
		fout<<names[i]<<endl;
	fout.close();
}

int main() {
//	preprocessing();
	unordered_map<string, int> alpha;
	alpha["A"]=1;
	alpha["B"]=2;
	alpha["C"]=3;
	alpha["D"]=4;
	alpha["E"]=5;
	alpha["F"]=6;
	alpha["G"]=7;
	alpha["H"]=8;
	alpha["I"]=9;
	alpha["J"]=10;
	alpha["K"]=11;
	alpha["L"]=12;
	alpha["M"]=13;
	alpha["N"]=14;
	alpha["O"]=15;
	alpha["P"]=16;
	alpha["Q"]=17;
	alpha["R"]=18;
	alpha["S"]=19;
	alpha["T"]=20;
	alpha["U"]=21;
	alpha["V"]=22;
	alpha["W"]=23;
	alpha["X"]=24;
	alpha["Y"]=25;
	alpha["Z"]=26;

	ifstream fin("p022_names_sort.txt");
	long result=0;
	int counter=1;
	string str;
	while(getline(fin, str)) {
		int sum=0;
		for(int i=0; i<str.length(); i++)
			sum+=alpha[str.substr(i, 1)];
		result+=sum*counter;
		counter++;
	}
	cout<<"Result is: "<<result<<endl;
	fin.close();
}
