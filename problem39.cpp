// We will use Euclid's formula to find Pythagorean triplets:
// a = m^2 - n^2
// b = 2mn
// c = m^2 + n^2
// here, m and n are positive integers with m>n

#include <iostream>
#include <cmath>
#include <unordered_map>
#include "utils.h"
using namespace std;

bool is_coprime(int num1, int num2) {
	if(num1<num2)
		if(num2%num1 == 0)
			return false;
	else
		if(num1%num2 == 0)
			return false;			
	unordered_map<int, int> fact1 = factors(num1);
	unordered_map<int, int> fact2 = factors(num2);
	bool is_coprime = true;
	for(auto iter = fact1.begin(); iter!=fact1.end(); iter++) {
		if(fact2.find(iter->first)!=fact2.end()) {
			is_coprime = false;
			break;
		}
	}
	return is_coprime;
}

int main() {

	int p = 1;
	unordered_map<int, int> result;
	while(p<=1000) {
		for(int n=1; n<=16; n++) { // Found this upper limit for n using n=m-1
			for(int m=n+1; m<=22; m++) { // Found this upper limit for m by setting n=1
				if((abs(m-n)%2==0) || !(is_coprime(m, n)))
					continue;
				for(int k=1; k<=100; k++) {
					int a = k*(pow(m, 2) - pow(n, 2));
					int b = k*2*m*n;
					int c = k*(pow(m, 2) + pow(n, 2));
					if((a+b+c)==p) {
						result[p]++;
					}
					else if((a+b+c)>p)
						break;
				}
			}
		}
		p++;
	}
	int max_p = 0;
	int value = 0;
	for(auto iter = result.begin(); iter!=result.end(); iter++) {
		if(iter->second>value) {
			max_p = iter->first;
			value = iter->second;
		}
	}
	cout<<"Perimeter with maximum number of Pythagorean triplet combinations: "<<max_p<<" with value: "<<value<<endl;
}
