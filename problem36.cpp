#include <iostream>
#include "utils.h"
using namespace std;

int main() {
	int sum = 0;
	for(int i=1; i<1000000; i++) {
		vector<int> num = num_to_vector(i);
		if(is_palindrome(num)) {
			vector<int> pal_bin = decimal_to_binary(i);
			if(is_palindrome(pal_bin))
				sum+=i;
		}
	}
	cout<<"Sum of all numbers less than 1 million whose decimal and binary representations both are palindromes is: "<<sum<<endl;
}
