#include <iostream>
using namespace std;

int main() {	
	cout<<"Enter the side of the grid square (should be odd, >=3) ";
	int size;
	int last_term=1;
	cin>>size;
	int sum=last_term;
	for(int i=3; i<=size; i+=2) {
		for(int j=0; j<4; j++) {
			last_term=last_term+(i-1);
			sum+=last_term;
		}
	}
	cout<<last_term<<endl<<sum<<endl;
}
