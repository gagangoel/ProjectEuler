class Problem2 {
	static int f1=1;
	static int f2=1;
	public static int fibonacci() {
		int result = f1+f2;
		f1=f2;
		f2=result;
		System.out.println(result);
		return result;
	}
	public static void main(String args[]) {
		int sum=0;
		int temp=0;
		for(int i=2;; i++) {
			temp = fibonacci();
			if((i%3)==2) {
				if(temp<=4000000)
					sum+=temp;
				else
					break;
			}
		}
		System.out.println("Sum is: " + sum);
	}
}
