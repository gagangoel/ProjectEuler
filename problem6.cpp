#include <iostream>
#include <cmath>
using namespace std;

int main() {
	int square_sum = 0;
	int sum_square = 0;
	for(int i = 1; i <=100; i++) {
		square_sum += i;
		sum_square+=pow(i, 2);
	}
	int result = pow(square_sum, 2) - sum_square;
	cout<<"Result is: "<<result<<endl;
}
