#include <iostream>
#include "utils.h"

using namespace std;

int main() {

  // Start from 3 digits since the solution to this problem cannot contain less than 3 digits
  int num_digits = 3;
  // Declare the number of primes in the family
  int num_prime_family = 8;

  bool found = false;

  while(!found) {
    vector<int> digits;
    // The solution cannot contain the wild entry element in the ones place, as we will not be able to construct the prime family of 6 or higher. Therefore setting i>=2 below
    for(int i=num_digits; i>=2; i--)
      digits.push_back(i);
    // Loop through the number of elements to be replaced. Then find all combinations of this loop variable in num_digits
    for(int i=1; (i<num_digits) && (!found); i++) {
      vector<vector<int>> digit_comb = combinations(digits, i);
      // bool to check if digit_comb contains the most siginificant digit. If yes, then the loop below will skip 0 otherwise it will render the number containing less than num_digits digits
      bool is_most_significant = false;
      for(int j=0; (j<digit_comb.size()) && (!found); j++) {
        for(auto iter : digit_comb[j]) {
          if(iter==num_digits)
            is_most_significant = true;
          cout<<iter<<" ";
        }
        cout<<endl;
        for(int num=pow(10, num_digits-1); (num<pow(10, num_digits)) && (!found); num++) {
          int cur_num = num;
          int num_prime_found=0;
          // Store the wild nums of the solution in a vector
          vector<int> wild_nums;
          int count=10;
          // We need to make sure that the number of remaining potential wild entries are >= number of primes yet to be found
          int temp_num_prime_family = num_prime_family;
          for(int k=0; (k<=9) && (count>=temp_num_prime_family); k++) {
            count--;
            if(k==0 && is_most_significant)
              continue;
            // Do an in-place replacement in cur_num at all wild indices, digit_comb[j], with a constant value, k
            for(auto l : digit_comb[j])
                cur_num = floor(cur_num/pow(10, l))*pow(10, l) + (k*pow(10, l-1)) + static_cast<int>(cur_num)%static_cast<int>(pow(10, l-1));
            if((is_prime(cur_num))) {
              temp_num_prime_family--;
              num_prime_found++;
              wild_nums.push_back(k);
            }
            if(num_prime_found==num_prime_family) {
              cout<<"Found the solution!"<<endl;
              cout<<"Base number is: "<<num<<endl;
              cout<<"Wild indices: ";
              for(auto m : digit_comb[j])
                cout<<m<<" ";
              cout<<endl<<"First wild element: "<<wild_nums[0]<<endl;
              found = true;
              break;
            }
          }
        }
      }
    }
    num_digits++;
  }
}            
