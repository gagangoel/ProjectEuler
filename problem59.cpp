#include <iostream>
#include <fstream>
#include "utils.h"

using namespace std;

int main() {

  vector<int> codes;
  ifstream fin("p059_cipher.txt");
  string code_string;
  while(getline(fin, code_string, ',')) {
    int code = stoi(code_string);
    codes.push_back(code);
  }
  fin.close();

  cout<<"Finding all combinations..."<<endl;
  // Here we are repeating the lower case characters 3 times since we also want to consider the case when characters in the password repeat.
  vector<int> characters = {97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122};
  vector<vector<int>> char_comb = combinations_fast(characters, 3);
  cout<<"Found all combinations!"<<endl;
  cout<<"Number of combinations are: "<<char_comb.size()<<endl;
  unordered_map<int, bool> seen;
  int num_valid_cases=0;
  for(int i=0; i<char_comb.size(); i++) {
    string temp;
    for(int j=0; j<char_comb[i].size(); j++)
      temp += to_string(char_comb[i][j]);
      int sum_ascii = 0;
      // We can further filter out the garbage values by counting the number of occurrences of the space character
      int count_spaces = 0;
    if(seen[stoi(temp)]==false) {
      string decrypt_text;
      bool contains_valid_chars = true;
      for(int j=0; j<codes.size(); j++) {
        int temp;
        if(j%3 == 0) {
          // XOR the encrypted code with the password key
          temp = codes[j] ^ char_comb[i][0];
          sum_ascii += temp;
          // Valid English characters should lie in the range >=32 and <=126
          if(temp<32 || temp>126) {
            contains_valid_chars = false;
            break;
          }
          char decrypt_code = temp;
          if(decrypt_code==' ')
            count_spaces++;
          decrypt_text.push_back(decrypt_code);
        }
        if(j%3 == 1) {
          temp = codes[j] ^ char_comb[i][1];
          sum_ascii += temp;
          if(temp<32 || temp>126) {
            contains_valid_chars = false;
            break;
          }
          char decrypt_code = temp;
          if(decrypt_code==' ')
            count_spaces++;
          decrypt_text.push_back(decrypt_code);
        }
        if(j%3 == 2) {
          temp = codes[j] ^ char_comb[i][2];
          sum_ascii += temp;
          if(temp<32 || temp>126) {
            contains_valid_chars = false;
            break;
          }
          char decrypt_code = temp;
          if(decrypt_code==' ')
            count_spaces++;
          decrypt_text.push_back(decrypt_code);
        }
      }
      // Count number of spaces found in the decrypted text. Since average length of an english word is 5.1 letters, there should be atleast 1 space after every 10 characters.
      if(contains_valid_chars && (count_spaces>=(codes.size()/10))) {
        cout<<"Current password: "<<temp<<endl;
        cout<<decrypt_text<<endl;
        cout<<"Sum of ASCII values of this text: "<<sum_ascii<<endl;
        num_valid_cases++;
      }
      seen[stoi(temp)] = true;
    }
  }
  cout<<"Total valid instances found: "<<num_valid_cases<<endl;
}
