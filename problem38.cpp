// Getting the code to find next permutation from narayana pandita's version for problem 24
// Finding the permutations starting with a given sequence in lexicographic order using Narayana Pandita's algorithm
#include <iostream>
#include <vector>
#include "utils.h"
using namespace std;

int main() {
	vector<int> permute {9, 1, 8, 2, 7, 3, 6, 4, 5};
	
// The integer number that whose concatenated product with {1, n} , n>1 forms a 9 digit pandigital number will have exactly 4 digits
//	Finding the last permutation
	vector<int> result(9);
	while(next_permute(permute)!=-1) {
		string four_digits;
		string five_digits;
		for(int i=0; i<permute.size(); i++) {
			if(i<=3)
				four_digits+=to_string(permute[i]);
			else
				five_digits+=to_string(permute[i]);
		}
		int four_dig_int = stoi(four_digits);
		int five_dig_int = stoi(five_digits);
		if(five_dig_int==(2*four_dig_int))
			result = permute;
	}
	cout<<"Result is: ";
	for(auto x:result)
		cout<<x;
	cout<<endl;
}
