#include <iostream>
#include <fstream>
#include <unordered_map>
#include <vector>

using namespace std;

int main() {
	unordered_map<char, int> letters;
	letters['A'] = 1;
	letters['B'] = 2;
	letters['C'] = 3;
	letters['D'] = 4;
	letters['E'] = 5;
	letters['F'] = 6;
	letters['G'] = 7;
	letters['H'] = 8;
	letters['I'] = 9;
	letters['J'] = 10;
	letters['K'] = 11;
	letters['L'] = 12;
	letters['M'] = 13;
	letters['N'] = 14;
	letters['O'] = 15;
	letters['P'] = 16;
	letters['Q'] = 17;
	letters['R'] = 18;
	letters['S'] = 19;
	letters['T'] = 20;
	letters['U'] = 21;
	letters['V'] = 22;
	letters['W'] = 23;
	letters['X'] = 24;
	letters['Y'] = 25;
	letters['Z'] = 26;
	ifstream fin("problem42_input.txt");
	vector<int> word_value;
	string str;
	int max_word_value = 0;
	while(getline(fin, str, ',')) {
		int temp = 0;
		for(int i=1; i<str.size()-1; i++)
			temp += letters[str[i]];
		word_value.push_back(temp);
		if(temp>max_word_value)
			max_word_value = temp;
	}
	fin.close();
	int sum = 1;
	int count = 2;
	unordered_map<int, bool> triangle_num;
	triangle_num[1] = true;
	while(sum < max_word_value) {
		sum += count;
		triangle_num[sum] = true;
		count++;
	}
	int result = 0;
	for(auto i : word_value) {
		if(triangle_num[i]==true)
			result++;
	}
	cout<<"Number of triangle words are: "<<result<<endl;
}
