#include <iostream>
#include "utils.h"

using namespace std;

// This function checks if the 0-9 pandigital has this property mentioned in the problem
bool has_property(vector<int> num) {
  unordered_map<int, int> prime;
  prime[1] = 2;
  prime[2] = 3;
  prime[3] = 5;
  prime[4] = 7;
  prime[5] = 11;
  prime[6] = 13;
  prime[7] = 17;
  string str;
  for(auto i : num)
    str += to_string(i);
  for(int i=1; i<=7; i++)
    if(stoi(str.substr(i, 3))%prime[i] != 0)
      return false;
  return true;
}

int main() {

  int start = 1023456789;
  vector<int> num = num_to_vector(start);
  long result = 0L;
  string temp;
  do {
    if(has_property(num)) {
      temp.clear();
      for(auto i : num)
        temp += to_string(i);
      result += stol(temp);
    }
  } while(next_permute(num)!=-1);
  cout<<"Sum of all 0-9 pandigital numbers with this property: "<<result<<endl;

}
