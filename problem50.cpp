#include <iostream>
#include "utils.h"

using namespace std;

int main() {

  int upper_limit = 1000000;

  vector<int> primes;
  // Find all primes under upper_limit
  int num = 1;
  while(num<upper_limit) {
    if(is_prime(num))
      primes.push_back(num);
    num++;
  }

  vector<vector<int>> result;
  // Setting the starting value for the window_length using heuristics. It is based on the upper_limit set earlier
  for(int window_length=primes.size()/100; window_length>0; window_length--) {
    // Setting the upper limit for i, again using heuristics. The solution should start in the first half of the elements of primes
    for(int i=0; (i<=(primes.size()-window_length)) && (i<=(primes.size()/2)); i++) {
      int temp=0;
      vector<int> temp_vec;
      // Exit out of the loop as soon as temp exceeds upper_limit
      for(int j=i; (j<(i + window_length)) && (temp<upper_limit); j++) {
        temp += primes[j];
        temp_vec.push_back(primes[j]);
      }
      if((temp<upper_limit) && is_prime(temp)) {
        result.push_back(temp_vec);
        break;
      }
    }
    if(result.size()!=0) {
      cout<<"We have "<<result.size()<<" solutions: "<<endl;
      break;
    }
  }
  for(int i=0; i<result.size(); i++) {
    cout<<(i+1)<<" solution has "<<result[i].size()<<" elements, and there sum is: ";
    int temp=0;
    for(auto j : result[i]) {
      temp += j;
      cout<<j<<" ";
    }
    cout<<endl;
    cout<<temp<<endl;
  }
}
