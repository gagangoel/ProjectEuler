#include <iostream>
#include <unordered_map>

using namespace std;

int main() {

  unordered_map<int, int> digits;

  bool found=false;
  int num = 1;
  while(!found) {
    digits.clear();
    int cur_num=num;
    while(cur_num!=0) {
      digits[cur_num%10]++;
      cur_num/=10;
    }
    found = true;
    for(int i=1; (i<=6) && (found); i++) {
      int num_multiple = i*num;
      unordered_map<int, int> multiple_digits;
      while(num_multiple!=0) {
        multiple_digits[num_multiple%10]++;
        num_multiple/=10;
      }
      if(digits.size()!=multiple_digits.size()) {
        found = false;
        break;
      }
      for(auto iter=multiple_digits.begin(); iter!=multiple_digits.end(); iter++) {
        if(digits[iter->first]!=iter->second) {
          found = false;
          break;
        }
      }
    }
    if(found) {
      cout<<"Solution is: "<<num<<endl;
      break;
    }
    num++;
  }
}
