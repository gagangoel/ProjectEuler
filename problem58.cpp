#include <iostream>
#include "utils.h"

using namespace std;

int main() {

  bool found = false;
  vector<int> diagonal = {1};
  vector<int> primes;
  int iter = 2;
  int square_size = 1;
  while(true) {
    square_size += 2;
    int count=1;
    int start = diagonal[diagonal.size() - 1];
    while(count<=4) {
      start += iter;
      diagonal.push_back(start);
      if(is_prime(start))
        primes.push_back(start);
      count++;
    }
    iter+=2;
    if((primes.size()*100)/diagonal.size() < 10) {
      cout<<"Found square size: "<<square_size<<endl;
      break;
    }
  }
}
