#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

int main() {

	string digits = "0123456789";
	vector<long> permuts;

	int counter=241920;
	long result=0;
	for(long i=2701345689; i<3000000000; i++) {
		string str = to_string(i);
		bool is_permut = true;
		for(int j=0; j<digits.length(); j++) {
			if(str.find(digits[j])!=string::npos)
				continue;
			else {
				is_permut = false;
				break;
			}
		}
		if(is_permut) {
			permuts.push_back(i);
			counter++;
		}
		if(counter==274240) {
			result=i;
			break;
		}			
	}
	cout<<result<<endl;
	cout<<permuts.size()<<endl;
}
