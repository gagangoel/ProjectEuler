#include <iostream>
#include "utils.h"
using namespace std;

int main() {
	int max = 1;
	for(int i=9; i>=1; i--) {
		int num = 1;
		string temp;
		while(num<=i) {
			temp+=to_string(num);
			num++;
		}
		vector<int> permute = num_to_vector(stoi(temp));
		if(is_prime(stoi(temp)))
			max = stoi(temp);
		while(next_permute(permute)!=-1) {
			temp.clear();
			for(int j=0; j<permute.size(); j++)
				temp+=to_string(permute[j]);
			if(is_prime(stoi(temp)))
				max = stoi(temp);
		}
		if(max!=1) {
			cout<<max<<endl;
			return 0;
		}
	}
}
