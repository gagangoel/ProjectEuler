#include <iostream>
#include <vector>
using namespace std;

int main() {
	cout<<"How long should the first number in the fibonacci series be (in digits)? ";
	int num_digits;
	cin>>num_digits;
	vector<int> current;
	vector<int> previous;
	current.push_back(1);
	previous.push_back(1);
	int counter=2;
	while(current.size()<num_digits) {
	int previous_sum=0, sum=0; // Since we are only summing 2 digits at a time, the sum can never be in 3 digits
	int i=0;
	vector<int> next;
	while(i<previous.size()) {
		sum = previous[i]+current[i]+(previous_sum/10);
		next.push_back(sum%10);
		previous_sum = sum;
		i++;
	}
	while(i<current.size()) {
		sum = current[i] + (previous_sum/10);
		next.push_back(sum%10);
		previous_sum = sum;
		i++;
	}
	if(sum>=10)
		next.push_back(sum/10);
	previous=current;
	current=next;
	counter++;
	}
	cout<<"Number is: ";
	for(int i=current.size()-1; i>=0; i--)
		cout<<current[i];
	cout<<endl;
	cout<<"Index of this number is: "<<counter<<endl;
}
