#include <iostream>
using namespace std;

int main() {
	cout<<"This program generates a prime number at nth position. Enter n (>=6)"<<endl;
	int pos = 0;
	cin>>pos;
	int counter = 5;
	bool is_prime = false;
	int prime = 0;
		for(int i=12; counter!=pos; i++) {
			is_prime = true;
			for(int j=2; j<=(i/j); j++) {
				if((i%j)==0) {
					is_prime = false;
					break;
				}
			}
			if(is_prime) {
				counter++;
				prime = i;
			}
		}
	cout<<"Prime number at Pos: "<<pos<<" is: "<<prime<<endl;
}
