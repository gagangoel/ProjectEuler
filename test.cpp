#include <iostream>
#include <vector>
#include <limits>
#include <unordered_map>
using namespace std;

int main() {
/*	cout<<(65^42)<<endl;
	char a = 97;
	char b = 'b';
	int b_int = b;
	cout<<a<<endl;
	cout<<b_int<<endl;
	exit(0);*/
	cout<<numeric_limits<int>::max()<<endl;
	cout<<numeric_limits<unsigned int>::max()<<endl;
	cout<<numeric_limits<long>::max()<<endl;
	cout<<numeric_limits<unsigned long>::max()<<endl;
	cout<<numeric_limits<unsigned long long>::max()<<endl;
	cout<<numeric_limits<size_t>::max()<<endl;
	cout<<numeric_limits<int>::min()<<endl;
	cout<<numeric_limits<long>::min()<<endl;
	string my_string = "012";
	int my_int = stoi(my_string);
	cout<<my_int<<endl;
	exit(0);
	enum type{t1, t2};
	type t3 = static_cast<type>(1);
	int testint = t1;
	cout<<testint<<endl;
	type t = t2;
	cout<<t<<endl;
	return 0;
	cout<<sizeof(long long int)<<endl;
	return 0;
	string strs = "helloworld";
	cout<<strs.substr(4)<<endl;
	return 0;
	int sum=0;
	for(int i=1; i<=100; i++)
		sum+=i;
	cout<<sum<<endl;
	return 0;
	vector<int> test_vector(51);
	cout<<test_vector.size()<<endl<<test_vector[1]<<endl;
	return 0;
	string strstr[] = {"str1", "str2"};
	cout<<sizeof(strstr)/sizeof(strstr[0])<<endl;
	return 0;

	unordered_map<int, int> months = {{1, 31}};
	cout<<months[1]<<endl;
	cout<<"Size of int is: "<<sizeof(int)<<endl;
	cout<<numeric_limits<int>::max()<<endl;
	cout<<numeric_limits<long>::max()<<endl;
	string str = "123";
	cout<<str[2]<<endl;
	int i = static_cast<int>(str[2]);
	cout<<i<<endl;
	int j = str[2]-48;
	cout<<j<<endl;
	vector<int> v = {1, 3, 5, 3, 2};
	cout<<"Vector size: "<<v.size()<<endl;
	int inttostr = 123;
	int counter = 0;
//	char* strfromint = new char[3];
	string strfromint="abc";
	while((inttostr%10) != inttostr) {
		cout<<"Number at 10^"<<counter<<" place is: "<<(inttostr%10)<<endl;
		strfromint[3-counter-1] = (inttostr%10) + 48; 
		inttostr/=10;
		counter++;
	}
	cout<<"Number at 10^"<<counter<<" place is: "<<(inttostr%10)<<endl;
	strfromint[3-counter-1] = (inttostr%10) + 48;
	cout<<strfromint<<endl;
}
