#include <iostream>
#include <vector>
#include <fstream>
using namespace std;

int main() {
// Reading the 100 50-digit numbers as an array of strings from an input file
	int num = 100;
	ifstream fin;
	fin.open("problem13_input.txt");
	int counter = 0;
	string str[101];
	while(!fin.eof()){
		getline(fin, str[counter]);
		cout<<str[counter]<<endl;
		counter++;
	}
	fin.close();
	counter=0;
	int sum=0;
	int sum_previous=0, sum_previous_previous=0;
	vector<int> v;

	for(int i=49; i>=0; i--) {
		counter=0;
		sum=0;
		while(counter<100) {
			sum+=(str[counter][i]-48) ;
			counter++;
		}
		sum+=(sum_previous_previous/100) + ((sum_previous/10)%10);
		cout<<sum<<endl;
		if(i==49) {
			sum_previous=sum;
			sum_previous_previous=0;
		}
		else {
			sum_previous_previous=sum_previous;
			sum_previous=sum;
		}
			v.push_back(sum%10);
	}

// For the 2nd most significant digit, sum_previous_previous' (which holds the value of sum_previous after the loop) 100th digit has to be added to the 10th place of the sum of the 1st most siginificant digit of every 50-digit number
	if(sum_previous_previous>=100)
		sum += 10*(sum_previous_previous/100);
		
	if(sum>=100) {
		v.push_back((sum/10)%10);
		v.push_back(sum/100);
	}
	else if(sum>=10 && sum<100)
		v.push_back(sum/10);
	
	cout<<endl<<"Number of elements in the sum is: "<<v.size()<<endl;

	cout<<"Sum of the numbers is: "<<endl;

	for(int i=v.size()-1; i>=0; i--)
		cout<<v[i];

}

