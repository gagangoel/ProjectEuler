#include <iostream>
#include "utils.h"
using namespace std;

int next_rotation(int num) {
	vector<int> result;
	while(num!=0) {
		result.push_back(num%10);
		num/=10;
	}
	vector<int> result2(result.size());
	copy(result.rbegin(), result.rend(), result2.begin());
	result=result2;
	result2.clear();
	int temp = result[result.size()-1];
	for(int i=result.size()-2; i>=0; i--) {
		result[i+1] = result[i];
	}
	result[0] = temp;
	string str="";
	for(auto i:result)
		str+=to_string(i);
	return stoi(str);
}

int main() {
	unordered_map<int, bool> result;
	for(int i=2; i<1000000; i++) {
		if(result[i]==true)
			continue;
		bool is_circular_prime;
		vector<int> other_rotations;
		if(is_prime(i)) {
			is_circular_prime=true;
			int size=0;
			int num=i;
			while(num!=0) {
				num/=10;
				size++;
			}
			num=i;
			for(int i=1; i<size; i++) {
				num=next_rotation(num);
				other_rotations.push_back(num);
				if(!is_prime(num)) {
					is_circular_prime=false;
					break;
				}
			}
		}
		else
			continue;
		if(is_circular_prime) {
			result[i]=true;
			for(auto j : other_rotations) {
//				cout<<j<<endl;
				result[j] = true;
			}
		}
	}
	cout<<"Number of circular primes below 1 million: ";
	int count=0;
	for(auto iter = result.begin(); iter!=result.end(); iter++)
		if(iter->second == true)
			count++;
	cout<<count<<endl;
}
