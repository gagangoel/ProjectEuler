#include <iostream>
#include <vector>
#include <unordered_map>
using namespace std;

int main() {
	int denom=2;
	vector<int> max_d;
	int result;
	int count = 0;
	while(denom<1000) {
		int remainder = 1%denom;
		unordered_map<int, bool> seen_remainder;
		vector<int> temp;
		while(seen_remainder[((remainder*10)%denom)]==false) {
			temp.push_back((remainder*10)/denom);
			remainder=(remainder*10)%denom;
			seen_remainder[remainder]=true;
		}
		// Checking for remainder 0 since we do not want to account for denoms which do not have a recurring cycle
		if((temp.size()>max_d.size()) && (seen_remainder[0]==false)) {
			count++;
			result=denom;
			max_d=temp;
//			cout<<denom<<endl;
		}
		denom++;
	}
	cout<<"Digits in decimal place are: ";
	for(int i=0; i<max_d.size(); i++)
		cout<<max_d[i];
	cout<<endl;
	cout<<result<<" is the number under 1000 with the longest recurring cycle in its decimal fraction part. Length of cycle: "<<max_d.size()<<endl;
	cout<<count<<endl;
}
