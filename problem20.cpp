#include <iostream>
#include <vector>
using namespace std;

int main() {
	cout<<"Enter the number for which to compute the factorial (>=2): ";
	int num;
	cin>>num;
	vector<int> fact;
	fact.push_back(1);
	for(int i=2; i<=num; i++) {
		int j=0;
		int lastprod=0, lastlastprod=0, lastlastlastprod=0, lastlastlastlastprod=0;
		while(j<fact.size()) {
			int temp = (i*fact[j])+((lastprod/10)%10)+((lastlastprod/100)%10)+((lastlastlastprod/1000)%10) + ((lastlastlastlastprod/10000)%10);
//			if(i==num)
//				cout<<temp<<endl;
			fact[j]=temp%10;
			lastlastlastlastprod = lastlastlastprod;
			lastlastlastprod = lastlastprod;
			lastlastprod = lastprod;
			lastprod = temp;
			j++;
		}
		int temp = lastprod + ((lastlastprod/100)%10)*10 + ((lastlastprod/1000)%10)*100 + ((lastlastprod/10000)%10)*1000 + ((lastlastlastprod/1000)%10)*10 + ((lastlastlastprod/10000)%10)*100 +  (lastlastlastlastprod/10000)*10;
		temp/=10;
		while(temp>0) {
			fact.push_back(temp%10);
			temp/=10;
		}
	}
	cout<<"Factorial is: ";
	for(int i=fact.size()-1; i>=0; i--)
		cout<<fact[i];
	cout<<endl;
	int sum=0;
	cout<<"Sum of the digits of the factorial: ";
	for(int i=0; i<fact.size(); i++)
		sum+=fact[i];
	cout<<sum<<endl;
 	cout<<"Number of digits: "<<fact.size()<<endl;
}
