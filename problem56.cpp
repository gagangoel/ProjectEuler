#include <iostream>
#include "utils.h"

using namespace std;

int main() {

  int maximum_sum=-1;
  int max_i = -1;
  int max_j = -1;
  vector<int> max_value;
  for(int i=1; i<100; i++) {
    for(int j=1; j<100; j++) {
      vector<int> num = num_power(i, j);
      int sum = 0;
      for(auto i : num)
        sum += i;
      if(maximum_sum < sum) {
        maximum_sum = sum;
        max_i = i;
        max_j = j;
        max_value = num;
      }
    }
  }
  cout<<"Maximum sum is: "<<maximum_sum<<endl;
  cout<<"Maximum number is: "<<max_i<<endl;
  cout<<"Maximum power is: "<<max_j<<endl;
  cout<<max_i<<"^"<<max_j<<": ";
  for(auto i : max_value)
    cout<<i;
  cout<<endl;
}
