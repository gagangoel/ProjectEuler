#include <iostream>
#include <unordered_map>

using namespace std;

int main() {
	unordered_map<int, bool> m;
	for(int i=1; i<1000000; i++)
		m[i]=false;

	int max_count=1;
	int max_element=1;
	for(long i=1L; i<1000000L; i++) {
		int counter=1;
		if(!m[i]) {
			m[i]=true;
			long num=i;
			while(num!=1) {
//				m[num]=true;
				if((num%2)==0)
					num/=2;
				else
					num=(3*num)+1;
				counter++;
				m[num]=true;
			}
		}
		if(max_count<counter) {
			max_count=counter;
			max_element=i;
		}
	}
	cout<<"Element under 1 million with longest chain satisfying Collatz conditions is: "<<max_element<<" with chain length: "<<max_count<<endl;
}
