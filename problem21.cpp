#include <iostream>
#include <unordered_map>
#include <vector>

using namespace std;

vector<int> factors(int elem) {
	vector<int> fact;
	fact.push_back(1);
	int i=2;
	while(i<=elem/i) {
		if((elem%i)==0) {
			fact.push_back(i);
			if(i!=(elem/i))
				fact.push_back(elem/i);
		}
		i++;
	}
	return fact;
}

int main() {
	unordered_map<int, bool> num;
	cout<<"Enter the upper limit: ";
	int limit;
	cin>>limit;
	for(int i=1; i<limit; i++)
		num[i]=false;
	vector<int> amicable;
	for(int i=1; i<limit; i++) {
		if(num[i])
			continue;
		else {
			num[i]=true;
			vector<int> fact = factors(i);
/*			cout<<"Factors of "<<i<<": ";
			for(int j=0; j<fact.size(); j++)
					cout<<fact[j]<<" ";
			cout<<endl;
			continue;*/
			int sum=0;
			for(int j=0; j<fact.size(); j++)
					sum+=fact[j];
			num[sum]=true;
			fact = factors(sum);
			int temp=sum;
			sum=0;
			for(int j=0; j<fact.size(); j++)
					sum+=fact[j];
			if(sum==i)
				if(temp!=i) {
					amicable.push_back(i);
					amicable.push_back(temp);
					cout<<"Amicable pair is: "<<i<<", "<<temp<<endl;
				}
		}
	}
	cout<<"Sum of all amicable numbers below "<<limit<<": ";
	int sum=0;
	for(int i=0; i<amicable.size(); i++)
		sum+=amicable[i];
	cout<<sum<<endl;
}
