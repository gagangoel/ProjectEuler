#include <iostream>
#include <vector>
#include <sstream>
#include <fstream>
#include <unordered_map>

using namespace std;


int player_rank(unordered_map<char, int> value, unordered_map<char, int> suit, unordered_map<char, int> values) {
  // Rank 1: Royal Flush
  if(suit.size()==1)
    if(value['T']==1 && value['J']==1 && value['Q']==1 && value['K']==1 && value['A']==1)
      return 1;
  // Rank 2: Straight Flush
  if(suit.size()==1) {
    vector<int> sort_values;
    for(auto iter=value.begin(); iter!=value.end(); iter++) {
      if(iter->second == 1)
        sort_values.push_back(iter->first);
    }
    sort(sort_values.begin(), sort_values.end());
    if((sort_values[1] == sort_values[0]+1) && (sort_values[2] == sort_values[0]+2) && (sort_values[3] == sort_values[0]+3) && (sort_values[4] == sort_values[0]+4))
      return 2;
  }
  // Rank 3: Four of a Kind
  for(auto iter = value.begin(); iter!=value.end(); iter++)
    if(iter->second == 4)
      return 3;
  // Rank 4: Full House
  bool three_of_a_kind=false;
  bool pair = false;
  for(auto iter = value.begin(); iter!=value.end(); iter++) {
    if(iter->second==2)
      pair=true;
    if(iter->second==3)
      three_of_a_kind=true;
  }
  if(three_of_a_kind && pair)
    return 4;
  // Rank 5: Flush
  if(suit.size()==1)
   return 5;
  // Rank 6: Straight
  vector<int> sort_values;
  for(auto iter=value.begin(); iter!=value.end(); iter++) {
    if(iter->second == 1)
      sort_values.push_back(iter->first);
  }
  sort(sort_values.begin(), sort_values.end());
  if((sort_values[1] == sort_values[0]+1) && (sort_values[2] == sort_values[0]+2) && (sort_values[3] == sort_values[0]+3) && (sort_values[4] == sort_values[0]+4))
    return 6;
  // Rank 7: Three of a kind
  if(three_of_a_kind)
    return 7;
  // Rank 8: Two Pairs
  for(auto iter = value.begin(); iter!=value.end(); iter++)
    if(iter->second == 2) {
      for(auto iter2=++iter; iter2!=value.end(); iter2++)
        if(iter2->second==2)
          return 8;
      break;
    }
  // Rank 9: One Pair
  if(pair)
    return 9;
  // In all other cases, we assign a Rank of 10
  return 10;
}

int main() {

unordered_map<char, int> values;
values['2'] = 1;
values['3'] = 2;
values['4'] = 3;
values['5'] = 4;
values['6'] = 5;
values['7'] = 6;
values['8'] = 7;
values['9'] = 8;
values['T'] = 9;
values['J'] = 10;
values['Q'] = 11;
values['K'] = 12;
values['A'] = 13;
  int player1_wins = 0;
  int player1_previous_win=0;

  ifstream fin("p054_poker.txt");
  string line;
  while(getline(fin, line)) {
    stringstream str(line);
    // Store the hands for player 1 and 2 in a vector, for each random draw from the input
    unordered_map<char, int> player1_value;
    unordered_map<char, int> player2_value;
    unordered_map<char, int> player1_suit;
    unordered_map<char, int> player2_suit;
    vector<int> player1_value_vector;
    vector<int> player2_value_vector;
    int count=1;
    string temp;
    while(str>>temp) {
      if(count<=5) {
        player1_value[temp[0]]++;
        player1_suit[temp[1]]++;
        player1_value_vector.push_back(values[temp[0]]);
      }
      else {
        player2_value[temp[0]]++;
        player2_suit[temp[1]]++;
        player2_value_vector.push_back(values[temp[0]]);
      }
      count++;
    }

    int player1_rank = player_rank(player1_value, player1_suit, values);
    int player2_rank = player_rank(player2_value, player2_suit, values);
    if(player1_rank < player2_rank)
      player1_wins++;
    else if(player1_rank == player2_rank) {
      if(player1_rank==10 || player1_rank==6 || player1_rank==5 || player1_rank==2) {
        sort(player1_value_vector.begin(), player1_value_vector.end());
        sort(player2_value_vector.begin(), player2_value_vector.end());
        bool player1_winner = false;
        for(int i=player1_value_vector.size()-1; i>=0; i--) {
          if(player1_value_vector[i]>player2_value_vector[i]) {
            player1_winner = true;
            break;
          }
          else if(player1_value_vector[i]<player2_value_vector[i]) {
            player1_winner = false;
            break;
          }
        }
        if(player1_winner)
          player1_wins++;
      }
      else if(player1_rank==3) {
        int player1_4 = -1;
        int player1_other = -1;
        int player2_4 = -1;
        int player2_other = -1;
        for(auto iter=player1_value.begin(); iter!=player1_value.end(); iter++)
          if(iter->second == 4)
            player1_4=values[iter->first];
          else
            player1_other=values[iter->first];
        for(auto iter=player2_value.begin(); iter!=player2_value.end(); iter++)
          if(iter->second == 4)
            player2_4=values[iter->first];
          else
            player2_other=values[iter->first];
        if((player1_4>player2_4) || (player1_4==player2_4 && player1_other>player2_other))
          player1_wins++;
      }
      else if(player1_rank==4) {
        int player1_3 = -1;
        int player1_2 = -1;
        int player2_3 = -1;
        int player2_2 = -1;
        for(auto iter=player1_value.begin(); iter!=player1_value.end(); iter++)
          if(iter->second == 3)
            player1_3=values[iter->first];
          else
            player1_2=values[iter->first];
        for(auto iter=player2_value.begin(); iter!=player2_value.end(); iter++)
          if(iter->second == 3)
            player2_3=values[iter->first];
          else
            player2_2=values[iter->first];
        if((player1_3>player2_3) || (player1_3==player2_3 && player1_2>player2_2))
          player1_wins++;
      }
      else if(player1_rank==7) {
        int player1_3 = -1;
        vector<int> player1_other;
        int player2_3 = -1;
        vector<int> player2_other;
        for(auto iter=player1_value.begin(); iter!=player1_value.end(); iter++)
          if(iter->second == 3)
            player1_3=values[iter->first];
          else
            player1_other.push_back(values[iter->first]);
        for(auto iter=player2_value.begin(); iter!=player2_value.end(); iter++)
          if(iter->second == 3)
            player2_3=values[iter->first];
          else
            player2_other.push_back(values[iter->first]);
        if(player1_3>player2_3)
          player1_wins++;
        else if(player1_3==player2_3) {
          sort(player1_other.begin(), player1_other.end());
          sort(player2_other.begin(), player2_other.end());
          bool player1_winner = false;
          for(int i=player1_other.size()-1; i>=0; i--) {
            if(player1_other[i]>player2_other[i]) {
              player1_winner = true;
              break;
            }
            else if(player1_other[i]<player2_other[i]) {
              player1_winner = false;
              break;
            }
          }
          if(player1_winner)
            player1_wins++;
        }
      }
      else if(player1_rank==8) {
        sort(player1_value_vector.begin(), player1_value_vector.end());
        sort(player2_value_vector.begin(), player2_value_vector.end());
        if(player1_value_vector[4] > player2_value_vector[4])
          player1_wins++;
        else if((player1_value_vector[4] == player2_value_vector[4]) && (player1_value_vector[2]>player2_value_vector[2]))
          player1_wins++;
        else if((player1_value_vector[2] == player2_value_vector[2]) && (player1_value_vector[0]>player2_value_vector[0]))
          player1_wins++;
      }
      else if(player1_rank==9) {
        int player1_2 = -1;
        vector<int> player1_other;
        int player2_2 = -1;
        vector<int> player2_other;
        for(auto iter=player1_value.begin(); iter!=player1_value.end(); iter++)
          if(iter->second == 2)
            player1_2=values[iter->first];
          else
            player1_other.push_back(values[iter->first]);
        for(auto iter=player2_value.begin(); iter!=player2_value.end(); iter++)
          if(iter->second == 2)
            player2_2=values[iter->first];
          else
            player2_other.push_back(values[iter->first]);
        if(player1_2>player2_2)
          player1_wins++;
        else if(player1_2==player2_2) {
          sort(player1_other.begin(), player1_other.end());
          sort(player2_other.begin(), player2_other.end());
          bool player1_winner = false;
          for(int i=player1_other.size()-1; i>=0; i--) {
            if(player1_other[i]>player2_other[i]) {
              player1_winner = true;
              break;
            }
            else if(player1_other[i]<player2_other[i]) {
              player1_winner = false;
              break;
            }
          }
          if(player1_winner)
            player1_wins++;
        }
      }
    }
  }
  cout<<"Player 1 win count: "<<player1_wins<<endl;
  fin.close();
}
