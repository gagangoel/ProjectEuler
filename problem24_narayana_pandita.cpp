// Finding the permutations starting with a given sequence in lexicographic order using Narayana Pandita's algorithm
#include <iostream>
#include <vector>
using namespace std;

int next_permute(vector<int>& permute) {

	int index1=-1;
	for(int i=permute.size()-1; i>0; i--) {
		if(permute[i-1]<permute[i]) {
			index1=i-1;
			break;
		}
	}
	int index2=-1;
	for(int i=permute.size()-1; i>index1; i--) {
		if(permute[i]>permute[index1]) {
			index2=i;
			break;
		}
	}	

	if((index1==-1)||(index2==-1))
		return -1;

// Swap elements at index1 and index2
	int temp=permute[index1];
	permute[index1]=permute[index2];
	permute[index2]=temp;

// Reversing the sequence of elements from index1+1 till n
	int j=0;
	for(int i=index1+1; i<=(index1+permute.size())/2; i++) {
		temp=permute[i];
		permute[i]=permute[permute.size()-j-1];
		permute[permute.size()-j-1]=temp;
		j++;
	}
	return 0;
}

int main() {
	vector<int> permute {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
//	for(int i=1; i<1000000; i++)
//		next_permute(permute);
//	Finding the last permutation
	while(next_permute(permute)!=-1)
		continue;
	for(const auto& i:permute)
		cout<<i;
	cout<<endl;
}
