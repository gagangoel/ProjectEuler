#include <iostream>
#include <vector>
using namespace std;

int main() {
	cout<<"To compute 2^(n), enter n(>=1): ";
	int power;
	cin>>power;
	vector<int> result;

	result.push_back(1);

	for(int i=1; i<=power; i++) {
		int carry_forward = 0;
		for(int j=0; j<result.size(); j++) {
			int temp = (2*result[j])+carry_forward;
			result[j] = temp%10;
			if(temp>=10)
				carry_forward=temp/10;
			else
				carry_forward=0;
		}
		if(carry_forward>0)
			result.push_back(carry_forward);
	}
	int sum=0;
	cout<<"Digits of 2^"<<power<<" are: ";
	for(int i=result.size()-1; i>=0; i--) {
		sum+=result[i];
		cout<<result[i];
	}
	cout<<endl;
	cout<<"Number of digits are: "<<result.size()<<endl;
	cout<<"And the sum of digits is: "<<sum<<endl;;
}
