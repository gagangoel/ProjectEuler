#include <iostream>
#include "utils.h"
using namespace std;

vector<int> reduce_fraction(unordered_map<int, int> fact_num, int num, unordered_map<int, int> fact_den, int den) {
	if(fact_num.size()==0)
		fact_num = {{num, 1}};
	if(fact_den.size()==0)
		fact_den = {{den, 1}};
	vector<int> result(2);
	for(auto i = fact_den.begin(); i!=fact_den.end(); i++) {
		if(fact_num[i->first]<i->second) {
			i->second -= fact_num[i->first];
			fact_num[i->first]=0;
		}
		else {
			fact_num[i->first]-=i->second;
			i->second = 0;
		}
	}
	result[0]=result[1]=1;
	for(auto i = fact_num.begin(); i!=fact_num.end(); i++)
		result[0]*=pow(i->first, i->second);
	for(auto i = fact_den.begin(); i!=fact_den.end(); i++)
		result[1]*=pow(i->first, i->second);
	return result;
}

int main() {
	// For numerator, we will only go till 98 as the problem requires the fraction be less than 1 in value and 2 digits in the num and denom
	for(int i=10; i<=98; i++) {
		unordered_map<int, int> fact_num = factors(i);
		int num1 = i/10; // numerator's first digit
		int num2 = i%10; // numberator's second digit
		for(int j=i+1; j<=99; j++) {
			unordered_map<int, int> fact_den = factors(j);
			vector<int> result = reduce_fraction(fact_num, i, fact_den, j);
			int den1 = j/10;
			int den2 = j%10;
			if((num1==den2) || (num2==den1) || (num1==den1) || (num2==den2)) {
				if(((num1==den2)&&(num2==den1)))
					continue;
				vector<int> reduced_result;
				if(num1==den2)
					reduced_result = reduce_fraction(factors(num2), num2, factors(den1), den1);
				else if(num2==den1)
					reduced_result = reduce_fraction(factors(num1), num1, factors(den2), den2);
				else if(num1==den1)
					reduced_result = reduce_fraction(factors(num2), num2, factors(den2), den2);
				else if(num2==den2)
					reduced_result = reduce_fraction(factors(num1), num1, factors(den1), den1);
				if((result[0]==reduced_result[0]) && (result[1]==reduced_result[1])) {
					if(num2==0 && den2==0)
						continue;
					cout<<i<<", "<<j<<endl;
				}
			}
		}
	}
}
