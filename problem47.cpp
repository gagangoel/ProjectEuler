#include <iostream>
#include "utils.h"

using namespace std;

bool are_prime_factors(const unordered_map<int, int>& factor) {
  for(auto iter=factor.begin(); iter!=factor.end(); iter++)
    if(!is_prime(iter->first))
      return false;
  return true;
}

int main() {
  // Number of consecutive integers for which to solve this problem
  int num = 4;
  int i=0;
  while(true) {
    int j=i;
    bool are_unique_factors = true;
    unordered_map<string, bool> factors_map;
    for(; (j<(i+num)) && (are_unique_factors); j++) {
      unordered_map<int, int> factor = factors(j);
      if((factor.size()==num) && (are_prime_factors(factor))) {
        // Now since the first 2 requirements of the problem are met, i.e., the factors are prime and the number of factors are equal to the number of consecituve integers. We now check if all these factors are distinct
        for(auto iter=factor.begin(); iter!=factor.end(); iter++) {
          // Creating our own key as a string concatenation of the first and second elements of the unordered_map
          string key = to_string(iter->first) + to_string(iter->second);
          if(factors_map[key] == false)
            factors_map[key] = true;
          else {
            are_unique_factors = false;
            break;
          }
        }
      }
      else
        break;
    }
    if((j==(i+num)) && (are_unique_factors)) {
      cout<<"The first number in a sequence of "<<num<<" consecutive integers to have "<<num<<" distinct prime factors each is: "<<i<<endl;
      break;
    }
    i++;
  }
}
