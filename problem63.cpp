#include <iostream>
#include <cmath>
#include "utils.h"

using namespace std;

int main() {

  // Upper limit on the number of digits in the problem will be 21 since 9^22 = 21 digits and 10^22 = 23 digits
  int num_digits = 21;
  long result = 0;
  vector<int> pow;
  while(num_digits >= 2) {
    for(int i=9; i>=2; --i) {
      pow = num_power(i, num_digits);
      if(pow.size() == num_digits)
        ++result;
      else
        break;
    }
    --num_digits;
  }
  result += 9;
  cout<<result<<endl;
}
