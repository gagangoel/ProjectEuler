#ifndef __UTILS__H__INCLUDED__
#define __UTILS__H__INCLUDED__
#include <vector>
#include <unordered_map>
#include <bitset>

std::unordered_map<int, int> factors(int);
std::vector<std::vector<int>> combinations(const std::vector<int>&, int);
bool is_prime(int);
bool is_palindrome(std::vector<int>);
std::vector<int> num_to_vector(int);
std::vector<int> decimal_to_binary(int);
int next_permute(std::vector<int>& permute);
int vector_to_int(const std::vector<int>&);
std::vector<int> factorial(int);
std::vector<int> vector_product(const std::vector<int>&, const std::vector<int>&);
std::vector<int> vector_sum(const std::vector<int>&, const std::vector<int>&);
std::vector<int> num_power(const int, const int);
// Prime Sieve using the algorithm of Sieve of Eratosthenes
template<int num>
std::vector<int> prime_sieve() {
  std::vector<int> result;
  std::bitset<num> prime_bits;
  int multiplier = 2;
  for(int i=2; i<=num; i++) {
    multiplier = 2;
    while((i*multiplier)<=num) {
      prime_bits.set(i*multiplier - 1);
      multiplier++;
    }
  }
  for(int i=1; i<prime_bits.size(); i++) {
    if(!prime_bits.test(i))
      result.push_back(i+1);
  }
  return result;
}

// A recursive solution to find nCk k-subsets using Lexicographic order.
// Also takes an optional argument that provides a custom implementation of the combinations function in utils.h which always adds the last number in the input vector. An efficient version of the original function for problems that only require the combinations formed from the addition of the new number to the input vector
template<typename T>
void combinations_fast_util(std::vector<std::vector<T>>& result, const std::vector<T>& input, int maxK, int start, int k, std::vector<int>& index, int input_size) {
  if(k>=maxK) {
    result.push_back(std::vector<T>(index.size()));
    for(int j=0; j<index.size(); j++)
      result[result.size()-1][j] = input[index[j]];
    return;
  }
  for(int i=start; i<input_size; i++) {
    index[k] = i;
    combinations_fast_util(result, input, maxK, i+1, k+1, index, input_size);
  }
  return;
}

template<typename T>
std::vector<std::vector<T>> combinations_fast(const std::vector<T>& input, int maxK, bool custom = false) {
  std::vector<std::vector<T>> result;
  if(maxK>input.size())
    return result;
  std::vector<int> index(maxK);
  int input_size = input.size();
  if(custom) {
    index[maxK-1] = input.size()-1;
    input_size = input.size() - 1;
    maxK--;
  }
  combinations_fast_util<T>(result, input, maxK, 0, 0, index, input_size);
  return result;
}
#endif
