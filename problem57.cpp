#include <iostream>
#include "utils.h"

using namespace std;

int main() {
  int num_iter=1000;
  int count = 1;
  vector<int> two = {2};
  int result = 0;
  while(count<=num_iter) {
    vector<int> numer = num_to_vector(1);
    vector<int> denom = num_to_vector(2);
    int inner_iter = count;
    while(inner_iter > 1) {
      numer = vector_sum(vector_product(two, denom), numer);
      swap(numer, denom);
      inner_iter--;
    }
    numer = vector_sum(denom, numer);
    if(numer.size() > denom.size()) { 
      result++;
    }
    count++;
  }
  cout<<"Number of fractions in which Numerator has more digits than Denominator: "<<result<<endl;
}
