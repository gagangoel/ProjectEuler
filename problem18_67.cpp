#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
using namespace std;

int main() {
//	ifstream fin("problem18_input.txt");
	ifstream fin("p067_triangle.txt");
	string str;
	vector<vector<int>> triangle;
	int counter=0;
	int temp;
	while(getline(fin, str)) {
		stringstream ss(str);
		triangle.push_back(vector<int>());
		while(ss>>temp)
			triangle[counter].push_back(temp);
		counter++;
	}
	fin.close();
	vector<vector<int>> result(triangle.size());
	result[triangle.size()-1]=triangle[triangle.size()-1];
	counter=triangle.size()-2;
	for(int i=triangle.size()-2; i>=0; i--) {
		for(auto j=0; j<triangle[i].size(); j++) {
			int sum=0;
			if(result[i+1][j]>result[i+1][j+1])
				sum = result[i+1][j]+triangle[i][j];
			else
				sum = result[i+1][j+1]+triangle[i][j];
			result[counter].push_back(sum);
		}
		counter--;
	}
	for(auto i=0; i<result.size(); i++) {
		for(auto j=0; j<result[i].size(); j++)
			cout<<result[i][j]<<" ";
		cout<<endl;
	}
}
