class Problem4 {
	public static boolean isPalindrome(int num) {
		String numStr = "" + num;
		boolean result = true;
		int length = numStr.length();
		for(int i=0; i<(length/2); i++) {
			if(numStr.charAt(i)!=numStr.charAt(length-1-i)) {
				result = false;
				break;
			}
		}
		return result;
	}

	public static void main(String args[]) {
//		int num = 9190919;
		int product=0;
		int maxPalindrome = 0;
		int multiple1 = 0;
		int multiple2 = 0;
		for(int i=1; i<=900; i++) {
			for(int j=1; j<=900; j++) {
				product=(1000-i)*(1000-j);
				if(isPalindrome(product))
					if(product>maxPalindrome) {
						multiple1 = 1000-i;
						multiple2 = 1000-j;
						maxPalindrome=product;
					}
			}
		}
		System.out.println("Largest palindrome formed by product of two 3-digit numbers is: " + maxPalindrome + ", with multiples " + multiple1 + ", " + multiple2);
	}
}
