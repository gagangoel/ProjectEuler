#include <iostream>

using namespace std;

int main() {
  int sum=0;
  for(int i=1; i<1000; i++) {
    if((i%3 == 0) || (i%5 ==0))
      sum += i;
  }
  cout<<"Sum of all numbers under 1000, divisible by 3 or 5: "<<sum<<endl;
}
