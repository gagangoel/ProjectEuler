#include <iostream>
using namespace std;

int main() {

	int num_factors=0;
	cout<<"Number the minimum number of factors for the triangle number: ";
	cin>>num_factors;
	int triangle = 0;
	int counter=0;
	for(int i=1; ; i++) {
		triangle += i;
		int j = 1;
		counter=0;
		while(j<=(triangle/j)) {
			if((triangle%j)==0) {
				if(j!=(triangle/j)) {
					counter+=2;
				}
				else {
					counter+=1;
				}
			}
			j++;
		}
		if(counter>num_factors)
			break;
	}
	cout<<"Triangle number: "<<triangle<<" has: "<<counter<<" factors."<<endl;
}
