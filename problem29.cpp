#include <iostream>
#include "utils.h"
using namespace std;

int main() {
	unordered_map<int, unordered_map<int, bool>> result;
	for(int i=2; i<=100; i++) {
		unordered_map<int, int> factor=factors(i);
		bool is_perfect=true;
		int power=1;
		int multiplier=1;
		if(factor.size()==0)
			is_perfect=false;
		else {
			power = factor.begin()->second;
			multiplier=1;
			for(auto iter=factor.begin(); iter!=factor.end(); iter++) {
				if(iter->second==power) {
					multiplier*=iter->first;
					continue;
				}
				else {
					is_perfect=false;
					break;
				}
			}
		}
		for(int j=2; j<=100; j++) {
			if(is_perfect)
				result[multiplier][power*j]=true;
			else
				result[i][j]=true;
		}
	}
	int sum=0;
	for(auto i=result.begin(); i!=result.end(); i++)
		for(auto j=(i->second).begin(); j!=(i->second).end(); j++) {
//			cout<<i->first<<", "<<j->first<<";";
			sum++;
		}
 //		cout<<endl;
	cout<<sum<<endl;
}
