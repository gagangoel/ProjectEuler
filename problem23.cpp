#include <iostream>
#include <vector>
#include <tr1/unordered_map>
using namespace std;
using namespace std::tr1;

// From problem21.cpp
vector<int> factors(int elem) {
	vector<int> fact;
	fact.push_back(1);
	int i=2;
	int temp=elem;
	while(i<temp) {
		if((elem%i)==0) {
			fact.push_back(i);
			if(i!=(elem/i))
				fact.push_back(elem/i);
			temp=elem/i;
		}
		i++;
	}
	return fact;
}

int main() {
	cout<<"Enter upper limit to find abundant numbers (>1): ";
	int limit;
	cin>>limit;
	unordered_map<int, bool> abundant;
	for(int i=1; i<limit; i++) {
		vector<int> fact = factors(i);
		int sum=0;
		for(int j=0; j<fact.size(); j++)
			sum+=fact[j];
		if(sum>i)
			abundant[i]=true;
	}
//	for(unordered_map<int, bool>::iterator j=abundant.begin(); j!=abundant.end(); j++)
	vector<int> result;
	for(int i=1; i<limit; i++) {
		bool in_result=true;
		for(unordered_map<int, bool>::iterator j=abundant.begin(); j!=abundant.end(); j++) {
			int temp=j->first;
//			cout<<"i is: "<<i<<" map is "<<temp<<endl;
			unordered_map<int, bool>::iterator finder = abundant.find(i-temp);
			if(finder!=abundant.end()) {
				in_result=false;
				break;
			}
		}
		if(in_result)
			result.push_back(i);
	}
	int sum=0;
	cout<<"Final result is: ";
	for(int i=0; i<result.size(); i++)
//		cout<<result[i]<<" ";
		sum+=result[i];
	cout<<sum<<endl;
}
