// To solve this problem, we find the maximum number that can satisfy this criterion. For the power of 4, maximum cannot exceed 99999. For the power of 5, maximum cannot exceed 999999
#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

int main() {
	int i=10;
	int power=5;
	vector<int> result;
	while(i<=999999) {
		int sum=0;
		int j=i;
		while(j!=0) {
			sum+=pow(j%10, power);
			j/=10;
		}
		if(sum==i)
			result.push_back(i);
		i++;
	}

	int sum=0;
	for(auto i:result) {
		sum+=i;
		cout<<i<<endl;
	}
	cout<<"Sum of the numbers is: "<<sum<<endl;
}
