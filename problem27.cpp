#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

// Reusing logic from Problem10 to determine whether a number is prime or not
inline bool is_prime(int i) {
	if(i==2)
		return true;
	bool is_prime = true;
//	Check if it's even
	if(i%2==0)
		return false;
	int j=3;
	while(j<=(i/j)) {
		if((i%j)==0) {
			is_prime = false;
			break;
		}
		j+=2; // Incrementing by 2 because division by even number is redundant as we know if a number if divisible by that even number, it has to be divisible by 2, for which we are already checking before the loop begins
	}
	return is_prime;
}

int main() {
	vector<int> primes;
	int max_i=0;
	int max_j=0;
	for(int i=-999; i<=999; i++) {
		for(int j=-1000; j<=1000; j++) {
			vector<int> prime;
			int n=0;
			while(true) {
				if(((pow(n, 2)+(i*n)+j)<1)||(is_prime(pow(n, 2)+(i*n)+j)==false))
					break;
				prime.push_back((pow(n, 2)+(i*n)+j));
				n++;
			}
			if(prime.size()>primes.size()) {
				max_i=i;
				max_j=j;
				primes=prime;
			}
		}
	}
	cout<<"Maximum coefficients are: a="<<max_i<<", b="<<max_j<<endl;
	cout<<"And the product is: "<<(max_i*max_j)<<endl;
}
