#include <iostream>
#include "utils.h"

using namespace std;

int main() {

  int num_digits_diff = 6;
  int result=0;
  for(int i=1; i<=100; i++) {
    // We only need to go half way, due to the symmetric properties of a combination
    for(int j=1; j<=(i/2); j++) {
      vector<int> n = factorial(i);
      vector<int> r = factorial(j);
      vector<int> n_minus_r = factorial(i-j);
      vector<int> prod = vector_product(r, n_minus_r);
      bool found = true;
      int size_diff = n.size()-prod.size();
      // If diff in number of digits of Num and Den is not >= 6, then this nCr does not satisfy the condition of this problem
      if(size_diff>=num_digits_diff) {
        // If size_diff > num_digits_diff, then we don't need to check the individual digits of the Num and Den
        if(size_diff==num_digits_diff) {
          prod.insert(prod.end(), size_diff, 0);
          for(int k=0; k<prod.size(); k++) {
            if(n[k]<prod[k]) {
              found = false;
              break;
            }
            else if(n[k]>prod[k])
              break;
          }
        }
      }
      else
        found = false;
      if(found) {
        result++;
        // Since we are only checking till half of the num, we also add the (n-r) value of nCr due to symmetry
        if(j!=(i-j))
          result++;
      }
    }
  }
  cout<<"Result is: "<<result<<endl;
}
