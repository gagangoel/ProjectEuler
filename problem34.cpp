#include <iostream>
using namespace std;

int factorial(int num) {
	if(num==0)
		return 1;
	return num*factorial(num-1);
}

int main() {
	int sum=0;
	for(int i=3; i<=9999999; i++) {
		int temp_sum=0;
		int num=i;
		while(num!=0) {
			temp_sum+=factorial(num%10);
			num/=10;
		}
		if(i==temp_sum)
			sum+=i;
	}
	cout<<"Sum of all numbers whose sum of factorials of the digits is equal to the number itself is: "<<sum<<endl;
}
