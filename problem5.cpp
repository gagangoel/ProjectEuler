#include <iostream>
#include <vector>
using namespace std;

int lcm(int a, int b) {
		int min = 0;
		vector<int> results;
		if(a<b) {
			if((b%a) == 0)
				return b;
			min = a;
		}
		else {
			if ((a%b) == 0)
				return a;
			min = b;
		}
//		for(int i = 2; i <= (min/2); i++) {
		for(int i = 2; i <= (min/i); i++) {
			while(((a%i)==0) && ((b%i)==0)) {
				a/=i;
				b/=i;
				results.push_back(i);
			}
		}
		int result = 1;
		for(int i = 0; i<results.size(); i++)
			result*=results[i];
		return result*a*b;
	}

int main() {
	cout<<"The lowest common multiple of all numbers from 1 to 20: "<<endl;
//	int a, b;
//	cin>>a>>b;
//	cout<<"LCM of " <<a<<" and "<<b<<" is: "<<lcm(a, b)<<endl;
	
	int temp_prod = 0;
	int counter=0;
	int result = lcm(1, 2);
	for(int i = 3; i <=20; i++) {
		result = lcm(i, result);
	}
	cout<<"LCM is: "<<result<<endl;
}
