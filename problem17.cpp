#include <iostream>
#include <fstream>
#include <tr1/unordered_map>
using namespace std;
using namespace std::tr1;

int main() {

	unordered_map<int, string> m;
	m[1]="one";
	m[2]="two";
	m[3]="three";
	m[4]="four";
	m[5]="five";
	m[6]="six";
	m[7]="seven";
	m[8]="eight";
	m[9]="nine";
	m[10]="ten";
	m[11]="eleven";
	m[12]="twelve";
	m[13]="thirteen";
	m[14]="fourteen";
	m[15]="fifteen";
	m[16]="sixteen";
	m[17]="seventeen";
	m[18]="eighteen";
	m[19]="nineteen";
	m[20]="twenty";
	m[30]="thirty";
	m[40]="forty";
	m[50]="fifty";
	m[60]="sixty";
	m[70]="seventy";
	m[80]="eighty";
	m[90]="ninety";
	
//	ofstream fout("problem17_output.txt");
	ofstream fout;
	fout.open("problem17_output.txt");
	string combined_str = "";
	for(int i=1; i<=1000; i++) {
		string str;
		if(i<=999 && i>=100) {
			str = m[i/100] + "hundred";
			if((i%100)!=0) {
				if(((i%100)<=19))
					str+="and" + m[i%100];
				else {
					if((i%100)%10!=0)
						str+="and" + m[((i%100)/10)*10] + "" + m[(i%100)%10];
					else
						str+="and" + m[i%100];
				}
			}
		}
		else if (i<100){
			if(i<=19)
				str = m[i];
			else {
				if((i%10)!=0)
					str = m[(i/10)*10] + m[i%10];
				else
					str = m[i];
			}
		}
		else
			str = "onethousand";
	fout<<str;
	fout<<endl;
	combined_str+=str;
	}
	fout<<endl;
	fout<<combined_str<<endl;
	fout.close();
	cout<<"Total length is: "<<combined_str.length()<<endl;
}
