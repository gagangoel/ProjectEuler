#include <iostream>
#include <unordered_set>
#include "utils.h"

using namespace std;

// Function to check if a given set of numbers satisfy the condition of the problem, i.e. taken any 2 numbers from the set and concatenating them in any order, is the result still prime or not
bool is_set_prime(const vector<int>& set, unordered_map<int, bool>& primes_lookup) {
  vector<vector<int>> comb = combinations_fast(set, 2);
  bool is_prime_set = true;
  for(int i=0; i<comb.size(); i++) {
    int num1 = comb[i][0];
    int num2 = comb[i][1];
    string num1_num2_str = to_string(num1) + to_string(num2);
    string num2_num1_str = to_string(num2) + to_string(num1);
    int num1_num2 = stoi(num1_num2_str);
    int num2_num1 = stoi(num2_num1_str);
    if(!primes_lookup[num1_num2]) {
//      is_prime_set = false;
        return false;
    }
//    else
//      primes_seen[num1_num2] = true;
    if(!primes_lookup[num2_num1]) {
//      is_prime_set = false;
        return false;
    }
//    else
//      primes_seen[num2_num1] = true;
  }
//  return is_prime_set;
    return true;
}

int main() {
  // DO NOT change the set_size, since we are using the set_union function
  int set_size = 2;
  int output_size = 5;
  bool found = false;
  const int num = 4000;
  vector<int> primes = prime_sieve<num>();
  cout<<"Found the prime sieve!"<<endl;
  vector<int> primes_large = prime_sieve<50000000>();
  cout<<"Found the prime sieve lookup!"<<endl;
  unordered_map<int, bool> primes_lookup;
  for(const auto& i : primes_large)
    primes_lookup[i] = true;
  vector<unordered_set<int>> primes_set;
  for(int i=0; i<primes.size(); i++)
    primes_set.push_back(unordered_set<int>({primes[i]}));
  int counter = 0;
  int max_set_size=0;
  vector<unordered_set<int>> primes_set_prev;
  int cur_set_size = 1;
  while(cur_set_size<output_size && (primes_set.size()!=0)) {
  vector<vector<unordered_set<int>>> comb = combinations_fast(primes_set, set_size);
  primes_set_prev = primes_set;
  primes_set.erase(primes_set.begin(), primes_set.end());
  vector<int>::iterator it;
  for(int i=0; i<comb.size(); i++) {
    vector<int> my_union(comb[i][0].size() + comb[i][1].size());
    it = set_union(comb[i][0].begin(), comb[i][0].end(), comb[i][1].begin(), comb[i][1].end(), my_union.begin());
    my_union.resize(it-my_union.begin());
    unordered_set<int> my_union_set_outer(my_union.begin(), my_union.end());
    if(find(primes_set.begin(), primes_set.end(), my_union_set_outer)!=primes_set.end())
      continue;
//    if(find(primes_set_prev.begin(), primes_set_prev.end(), my_union_set)!=primes_set_prev.end())
//      continue;
    vector<vector<int>> my_union_comb;
    if(my_union.size()>(cur_set_size+1))
      my_union_comb = combinations_fast(my_union, cur_set_size+1);
    else if(my_union.size()==(cur_set_size+1))
      my_union_comb.push_back(my_union);
    else
      continue;
    for(int j=0; j<my_union_comb.size(); j++) {
    unordered_set<int> my_union_set(my_union_comb[j].begin(), my_union_comb[j].end());
    if(find(primes_set.begin(), primes_set.end(), my_union_set)!=primes_set.end())
      continue;
    if(is_set_prime(my_union_comb[j], primes_lookup)) {
      primes_set.push_back(my_union_set);
      if(max_set_size<my_union_comb[j].size()) {
        cout<<"Maximum set size found: "<<my_union_comb[j].size()<<endl;
        max_set_size = my_union_comb[j].size();
      }
//      if(my_union.size()>=output_size) {
      if((cur_set_size+1)==output_size) {
      cout<<"Found the result!"<<endl;
      for(const auto& k : my_union_comb[j])
        cout<<k<<endl;
      found = true;
//      break;
      }
    }
  }
  }
  cur_set_size++;
  }
}
