#include <iostream>
#include "utils.h"
using namespace std;

int find_combinations(vector<int> subset, int required_sum, int count) {
	int sum=0;
	for(auto i:subset)
		sum+=i;
	required_sum-=sum;
	if(required_sum==0) {
		count++;
		return count;
	}
	else if(required_sum<0)
		return count;

	for(int i=1; i<=subset.size(); i++) {
		vector<vector<int>> combination = combinations(subset, i);
		for(auto j:combination) {
			count=find_combinations(j, required_sum, count);
		}
	}
	return count;
}	

int main() {
//	vector<int> input = {1, 2, 5, 10, 20, 50, 100};
/*	vector<vector<int>> binary = combinations(input, 2);
	for(int i=0; i<binary.size(); i++) {
		for(int j=0; j<binary[i].size(); j++)
			cout<<binary[i][j]<<" ";
		cout<<endl;
	}
*/

	vector<int> input = {1, 2, 5, 10, 20, 50, 100};
	int required_sum=200;
	int count=0;
	for(int i=1; i<=input.size(); i++) {
		vector<vector<int>> combination = combinations(input, i);
		for(auto j:combination) {
			count=find_combinations(j, required_sum, count);
		}
	}
	cout<<"Total possible ways are: "<<count<<endl;
}
