#include "split.h"
#include <iostream>
#include <unordered_map>
#include <vector>
using namespace std;

int main() {
/*	cout<<"Enter a string: ";
	string str;
	getline(cin, str);
	cout<<"Enter a delimiter character: ";
	char delim;
	cin.get(delim);
	vector<string> split_string = split(str, delim);
	for(auto i=0; i<split_string.size(); i++)
		cout<<split_string[i]<<endl;
*/
	cout<<"Finding number of days since Jan 1, 1900..."<<endl;
	int year=1901;
	int max_year=2000;
	bool is_leap;
	int num_days=365;
	int num_sundays=0;
	unordered_map<int, int> months = {{1, 31}, {2, 28}, {3, 31}, {4, 30}, {5, 31}, {6, 30}, {7, 31}, {8, 31}, {9, 30}, {10, 31}, {11, 30}, {12, 31}};
	for(int i=year; i<=max_year; i++) {
		is_leap=false;
		if((i%400)==0)
			is_leap=true;
		else if(((i%4)==0) && (i%100)!=0)
			is_leap=true;
		else
			is_leap=false;
		for(int j=1; j<=12; j++) {
			if((num_days%7) == 6) {
				cout<<"Year: "<<i<<" Month: "<<j<<" Num days: "<<num_days<<endl;
				num_sundays++;
			}
			if((j==2) && is_leap)
				num_days+=29;
			else
				num_days+=months[j];
		}
	}
	cout<<"Number of Sundays on the first of the month since "<<year<<" until year "<<max_year<<": "<<num_sundays<<endl;
}
