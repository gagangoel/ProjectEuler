#include <iostream>
#include <cmath>
#include <vector>
#include "utils.h"

using namespace std;

unordered_map<int, int> factors(int num) {
	unordered_map<int, int> result;
	int i=2;
	int init_num=num;
	while((num>1) && (i!=init_num)) {
		while(num%i==0) {
			result[i]+=1;
			num/=i;
		}
		i++;
	}
	return result;
}


vector<vector<int>> combinations(const vector<int>& input, int num) {
	vector<vector<int>> result;
	if(input.size()<num)
		return result;

//	Finding binary representation of all numbers from 0 to (2^(size of input))-1
	vector<vector<int>> binary;
	for(int i=0; i<pow(2, input.size()); i++) {
		binary.push_back(vector<int>(input.size()));
		vector<int> temp;
		int k=i;
		while(k>=1) {
			temp.push_back(k%2);
			k/=2;
		}
		int index=input.size()-temp.size();
		for(auto iter=temp.rbegin(); iter!=temp.rend(); iter++) {
			binary[i][index]=*iter;
			index++;
		}
	}

	int index=0;
	for(auto i:binary) {
		int count=0;
		for(auto j:i)
			if(j==1)
				count++;
		if(count==num) {
			result.push_back(vector<int>());
			for(auto j=0 ; j<i.size(); j++)
				if(i[j]==1) {
					result[index].push_back(input[j]);
				}
			index++;
		}
	}
	return result;
}

bool is_prime(int num) {
	if(num==2)
		return true;
	if(num==1 || (num%2==0))
		return false;
	bool is_prime=true;
	int initial_num=num;
	for(int i=3; i<=num; i+=2) {
		if(initial_num%i==0) {
			is_prime=false;
			break;
		}
		num=initial_num/i;
	}
	return is_prime;
}

bool is_palindrome(vector<int> num) {
	if(num.size()<2)
		return true;
	for(int i=0; i<num.size()/2; i++)
		if(num[i]!=num[num.size()-1-i])
			return false;
	return true;
}

vector<int> num_to_vector(int num) {
	vector<int> result;
	while(num!=0) {
		result.push_back(num%10);
		num/=10;
	}
	vector<int> result2(result.size());
	copy(result.rbegin(), result.rend(), result2.begin());
	result=result2;
	result2.clear();
	return result;
}

vector<int> decimal_to_binary(int num) {
	vector<int> result;
	while(num!=1) {
		result.push_back(num%2);
		num/=2;
	}
	result.push_back(num);
	vector<int> result2(result.size());
	copy(result.rbegin(), result.rend(), result2.begin());
	result=result2;
	result2.clear();
	return result;
}

//Implementing Narayana Pandita's algorithm to find the next permutation
int next_permute(vector<int>& permute) {

	int index1=-1;
	for(int i=permute.size()-1; i>0; i--) {
		if(permute[i-1]<permute[i]) {
			index1=i-1;
			break;
		}
	}
	int index2=-1;
	for(int i=permute.size()-1; i>index1; i--) {
		if(permute[i]>permute[index1]) {
			index2=i;
			break;
		}
	}	

	if((index1==-1)||(index2==-1))
		return -1;

// Swap elements at index1 and index2
	int temp=permute[index1];
	permute[index1]=permute[index2];
	permute[index2]=temp;

// Reversing the sequence of elements from index1+1 till n
	int j=0;
	for(int i=index1+1; i<=(index1+permute.size())/2; i++) {
		temp=permute[i];
		permute[i]=permute[permute.size()-j-1];
		permute[permute.size()-j-1]=temp;
		j++;
	}
	return 0;
}

// Function that return an int from a vector
int vector_to_int(const vector<int>& vec) {
  string temp;
  for(int i : vec)
    temp += to_string(i);
  return stoi(temp);
}

// Based on the solution to Problem 20. Computes factorial for a 2 <= num <= 10000
vector<int> factorial(int num) {

  vector<int> fact;
  fact.push_back(1);
  for(int i=2; i<=num; i++) {
    int j=0;
    int lastprod=0, lastlastprod=0, lastlastlastprod=0, lastlastlastlastprod=0;
    while(j<fact.size()) {
      int temp = (i*fact[j])+((lastprod/10)%10)+((lastlastprod/100)%10)+((lastlastlastprod/1000)%10) + ((lastlastlastlastprod/10000)%10);
//                      if(i==num)
//                              cout<<temp<<endl;
      fact[j]=temp%10;
      lastlastlastlastprod = lastlastlastprod;
      lastlastlastprod = lastlastprod;
      lastlastprod = lastprod;
      lastprod = temp;
      j++;
    }
    int temp = lastprod + ((lastlastprod/100)%10)*10 + ((lastlastprod/1000)%10)*100 + ((lastlastprod/10000)%10)*1000 + ((lastlastlastprod/1000)%10)*10 + ((lastlastlastprod/10000)%10)*100 +  (lastlastlastlastprod/10000)*10;
    temp/=10;
    while(temp>0) {
      fact.push_back(temp%10);
      temp/=10;
    }
  }
  vector<int> fact_temp(fact.size());
  copy(fact.rbegin(), fact.rend(), fact_temp.begin());
  fact = fact_temp;
  return fact;
} 

vector<int> vector_product(const vector<int>& m1, const vector<int>& m2) {
  vector<int> result;
  int count1=0;
  int count2=0;
  int last_prod_sum=0;
  int temp=0;
  for(int i=m2.size()-1; i>=0; i--) {
    last_prod_sum = 0;
    count2=0;
    for(int j=m1.size()-1; j>=0; j--) {
      if(count1==0) {
        temp = m2[i]*m1[j] + (last_prod_sum/10)%10;
        result.push_back(temp%10);
        last_prod_sum = temp;
      }
      else {
        if(result.size()>(count1+count2)) {
          temp = m2[i]*m1[j] + result[count2+count1] + (last_prod_sum/10)%10;
          result[count2+count1] = temp%10;
          last_prod_sum = temp;
        }
        else {
          temp = m2[i]*m1[j] + (last_prod_sum/10)%10;
          result.push_back(temp%10);
          last_prod_sum = temp;
        }
      }
      count2++;
    }
    last_prod_sum /= 10;
    if(last_prod_sum!=0)
      result.push_back(last_prod_sum%10);
    count1++;
  }
  vector<int> result_reverse(result.size());
  copy(result.rbegin(), result.rend(), result_reverse.begin());
  result = result_reverse;
  return result;
}

vector<int> vector_sum(const vector<int>& v1, const vector<int>& v2) {
  vector<int> result;
  vector<int> big;
  vector<int> small;
  if(v1.size() < v2.size()) {
    big = v2;
    small = v1;
  }
  else {
    big = v1;
    small = v2;
  }
  int last_sum=0;
  int sum=0;
  for(int i=small.size()-1; i>=0; i--) {
    sum = big[big.size()-small.size()+i]+small[i]+(last_sum/10)%10;
    result.push_back(sum%10);
    last_sum=sum;
  }
  for(int i=big.size()-small.size()-1; i>=0; i--) {
    sum = big[i] + (last_sum/10)%10;
    result.push_back(sum%10);
    last_sum = sum;
  }
  last_sum /= 10;
  while(last_sum!=0) {
    result.push_back(last_sum%10);
    last_sum /= 10;
  }
  vector<int> reverse(result.size());
  copy(result.rbegin(), result.rend(), reverse.begin());
  return reverse;
}

// Re-using code from Problem 48
vector<int> num_power(const int num, const int power) {
  vector<int> fact;
  fact.push_back(1);
  int count=1;
  while(count<=power) {
    int j=0;
    int lastprod=0, lastlastprod=0, lastlastlastprod=0, lastlastlastlastprod=0;
    while(j<fact.size()) {
      int temp = (num*fact[j])+((lastprod/10)%10)+((lastlastprod/100)%10)+((lastlastlastprod/1000)%10) + ((lastlastlastlastprod/10000)%10);
      fact[j]=temp%10;
      lastlastlastlastprod = lastlastlastprod;
      lastlastlastprod = lastlastprod;
      lastlastprod = lastprod;
      lastprod = temp;
      j++;
    }
    int temp = lastprod + ((lastlastprod/100)%10)*10 + ((lastlastprod/1000)%10)*100 + ((lastlastprod/10000)%10)*1000 + ((lastlastlastprod/1000)%10)*10 + ((lastlastlastprod/10000)%10)*100 +  (lastlastlastlastprod/10000)*10;
    temp/=10;
    while(temp>0) {
      fact.push_back(temp%10);
      temp/=10;
    }
    count++;
  }
  vector<int> fact_temp(fact.size());
  copy(fact.rbegin(), fact.rend(), fact_temp.begin());
  return fact_temp;
}
