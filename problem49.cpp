#include <iostream>
#include "utils.h"

using namespace std;

int main() {

  // The elements of the arithematic sequence can have upto 2 repeating digits. It cannot have more than 2 repeating digits as in that case, we cannot construct an arithematic sequence.
  vector<int> digits = {0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 6, 7, 7, 8, 8, 9, 9};
  // Since we are only interested in 4 digit numbers, so set the 2nd arg to 4 below
  // The ordering of a elements in the vector of vectors returned by combinations() will be sorted since the input is sorted
  vector<vector<int>> comb = combinations(digits, 4);
  int count=0;

  for(int i=0; i<comb.size(); i++) {
    vector<int> result;
    int num = vector_to_int(comb[i]);
    // Checking for num to be > 1000 since we are only interested in 4-digit numbers
    if(is_prime(num) && (num>1000))
      result.push_back(num);
    while(next_permute(comb[i])!=-1) {
      num = vector_to_int(comb[i]);
      if(is_prime(num) && (num>1000))
        result.push_back(num);
    }
    // Now that we have a candidate list of numbers, we will look for all possible combinations of a pair of such numbers
    vector<vector<int>> result_comb = combinations(result, 2);
    unordered_map<int, int> difference;
    for(int j=0; j<result_comb.size(); j++) {
      // Increment count of the diff by one everytime we find the same diff in our combination
      difference[result_comb[j][1]-result_comb[j][0]]++;
    }
    bool found = false;
    int final_difference;
    for(auto iter=difference.begin(); iter!=difference.end() && !found; iter++) {
      // Iterate through the hash map of diffs and check if for any diff, the count is equal to 2
      if(iter->second == 2) {
        final_difference = iter->first;
        // Now that we have found the candidate resulting diff, we now iterate through our original list of 4-digit elements to check if this diff can construct an arithematic sequence
        for(int j=0; j<result.size()-2; j++) {
          // In our candidate list, we check if <current value + diff> and <current value + 2*diff> exist. If yes, then <current value> is the first element of the trequired arithematic sequence.
          if((find(result.begin(), result.end(), (result[j]+final_difference))!=result.end()) && (find(result.begin(), result.end(), (result[j]+final_difference+final_difference))!=result.end())) {
            cout<<"First element of the resulting arithematic sequence is: "<<result[j]<<endl;
            cout<<"Add "<<final_difference<<" to this element to get the next 2 elements"<<endl;
            found = true;
            count++;
            break;
          }
        }
      }
    }
  }
  cout<<count<<endl;
}
