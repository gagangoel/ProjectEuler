#include <iostream>
#include <vector>
#include <unordered_map>

using namespace std;

int main() {

  vector<long> t;
  unordered_map<long, bool> t_map, h_map, p_map;
  long index = 1L;
  int count=0;
  while(count!=3) {
    long t_temp = index*(index+1)/2;
    long p_temp = index*(3*index - 1)/2;
    long h_temp = index*(2*index - 1);
    t.push_back(t_temp);
    t_map[t_temp] = true;
    p_map[p_temp] = true;
    h_map[h_temp] = true;
    if((p_map[t_temp]==true) && (h_map[t_temp]==true)) {
      cout<<"Found the triangle number that is also pentagonal and hexagonal: "<<t_temp<<" at index: "<<index<<endl;
      count++;
    }
    index++;
  }
}
