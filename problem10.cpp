#include <iostream>
using namespace std;

int main() {
	cout<<"Sum of all prime numbers below 2 million: ";
	long sum = 5L;
	bool is_prime;
	for(int i=5; i<2000000; i+=2) {
		is_prime = true;
		int j=3;
		while(j<=(i/j)) {
//		for(int j=3; j<(i/2); j+=2) {
			if((i%j)==0) {
				is_prime = false;
				break;
			}
			j+=2; // Incrementing by 2 because division by even number is redundant as we know if a number if divisible by that even number, it has to be divisible by 2, for which we are not checking since we all prime numbers (except 2) are odd
		}
			if(is_prime)
				sum+=i;
//				cout<<"Found a prime number: "<<i<<endl;
	}
	cout<<sum<<endl;
}
