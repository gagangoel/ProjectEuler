#include <iostream>
#include "utils.h"

using namespace std;

int main() {

  int result = 0;
  int num=0;
  int num_iterations=50;
  while(num<10000) {
    int iter;
    vector<int> prev_vec = num_to_vector(num);
    bool is_lychrel = true;
    for(iter=1; iter<num_iterations; iter++) {
      vector<int> vec_rev(prev_vec.size());
      copy(prev_vec.rbegin(), prev_vec.rend(), vec_rev.begin());
      vector<int> cur_vec = vector_sum(prev_vec, vec_rev);
      if(is_palindrome(cur_vec)) {
        is_lychrel = false;
        break;
      }
      prev_vec = cur_vec;
    }
    if(is_lychrel) {
      cout<<num<<" is Lychrel"<<endl;
      result++;
    }
    num++;
  }
  cout<<"Total number of Lychrel numbers under 10000 are: "<<result<<endl;
}
