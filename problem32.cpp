// The product will always have 4 digits to satisfy the requirement of the problem in which the mutiplicand, multiplier and the product form a 1 through 9 pandigital (an n-digit number is pandigital if it contains all the digits from 1 through n exactly once). So, the lowest possible product would be 1234 and highest would be 9876

#include <iostream>
#include "utils.h"
using namespace std;

bool if_duplicate(int num) {
	string num_str = to_string(num);
	unordered_map<char, bool> digits;
	bool found=false;
	for(auto l:num_str) {
		if(digits[l]==true) {
			found=true;
			break;
		}
		else
			digits[l] = true;
	}
	return found;
}

bool is_pandigital(int num) {
	bool found = if_duplicate(num);
	bool is_pandigital = false;
	string num_str = to_string(num);
	if(found==false) {
		is_pandigital = true;
		for(int i=1; i<=num_str.size(); i++) {
			auto found2 = num_str.find(to_string(i));
			if(found2==string::npos) {
				is_pandigital=false;
				break;
			}
		}
	}
	else
		is_pandigital = false;
	return is_pandigital;
}
int main() {
	vector<int> result;
	for(int k=1234; k<=9876; k++) {
		if(if_duplicate(k))
			continue;

		unordered_map<int, int> factor = factors(k);
		vector<int> fact;
		for(auto i=factor.begin(); i!=factor.end(); i++)
			for(int j=1; j<=i->second; j++)
				fact.push_back(i->first);
		bool found=false;
		for(int i=1; i<fact.size(); i++) {
			vector<vector<int>> combin = combinations(fact, i);
			for(auto j:combin) {
				int multiplier = 1;
				for(auto k:j)
					multiplier*=k;
				if(if_duplicate(multiplier))
					continue;
				int multiplicand = k/multiplier;
				if(if_duplicate(multiplicand))
					continue;
				if(is_pandigital(stoi(to_string(k) + to_string(multiplier) + to_string(multiplicand)))) {
					cout<<k<<", "<<multiplier<<", "<<multiplicand<<endl;
					found = true;
					// Now we need to make sure that this pandigital number contains 9 digits. We can do this by checking if either the product, multiplier or the multiplicand contain "9". This check is sufficient since we have already made sure that these 3 entries make a pandigital number
					if((to_string(k).find("9")!=string::npos) || (to_string(multiplier).find("9")!=string::npos) || (to_string(multiplicand).find("9")!=string::npos))
						result.push_back(k);
					break;
				}
			}
			if(found)
				break;
		}
	}
	int sum=0;
	for(auto i:result) {
		sum+=i;
		cout<<i<<endl;
	}
	cout<<"Sum of all numbers whose multiplier, multiplicand and the product together compose a 1 through 9 pandigital is: "<<sum<<endl;
}	
