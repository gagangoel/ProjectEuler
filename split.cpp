#include "split.h"
#include <string>
#include <sstream>

using namespace std;

vector<string> split(const string & str, char delim) {
	vector<string> result;
	split(str, delim, result);
	return result;
}

void split(const string & str, char delim, vector<string>& result) {
	stringstream ss(str);
	string buf;
	while(getline(ss, buf, delim))
		result.push_back(buf);
}
