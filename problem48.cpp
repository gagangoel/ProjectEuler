#include <iostream>
#include <vector>

using namespace std;

// Re-using code from Problem 20 that computes the factorial upto 10000. This problem is very similar. Instead of a incrementing/decrementing the number across multiple iterations, we keep it constant
long last_10_digits(int num) {

  vector<int> fact;
  fact.push_back(1);
  int count=1;
  while(count<=num) {
                int j=0;
                int lastprod=0, lastlastprod=0, lastlastlastprod=0, lastlastlastlastprod=0;
                while(j<fact.size()) {
                        int temp = (num*fact[j])+((lastprod/10)%10)+((lastlastprod/100)%10)+((lastlastlastprod/1000)%10) + ((lastlastlastlastprod/10000)%10);
//                      if(i==num)
//                              cout<<temp<<endl;
                        fact[j]=temp%10;
                        lastlastlastlastprod = lastlastlastprod;
                        lastlastlastprod = lastlastprod;
                        lastlastprod = lastprod;
                        lastprod = temp;
                        j++;
                }
                int temp = lastprod + ((lastlastprod/100)%10)*10 + ((lastlastprod/1000)%10)*100 + ((lastlastprod/10000)%10)*1000 + ((lastlastlastprod/1000)%10)*10 + ((lastlastlastprod/10000)%10)*100 +  (lastlastlastlastprod/10000)*10;
                temp/=10;
                while(temp>0) {
                        fact.push_back(temp%10);
                        temp/=10;
                }
    count++;
  }

// The problem asks for last 10 digits of the result
  string temp;
  for(int i=9; i>=0; i--) {
    if(fact.size()<=i)
      continue;
    temp+=to_string(fact[i]);
  }

// Storing the digits as a long as the final result will not fit into an int
  return stol(temp);

}

int main() {
  long result;
  for(int i=1; i<=1000; i++)
    result+=last_10_digits(i);
  cout<<"Last 10 digits of the series, 1^1 + 2^2 + ... + 1000^1000 is: "<<result<<endl;
}
