#include <iostream>
#include <vector>

using namespace std;

int count(vector<int> coins, int money) {

  if (money == 0)
    return 1;
  else if ((money > 0) && (!coins.empty()))
    return count(coins, money - coins[0]) + count(vector<int>(coins.begin() + 1, coins.end()), money);
  else
    return 0;

}

int main() {

  vector<int> coins = {1, 2, 5, 10, 20, 50, 100};
  int required_sum = 200;
  int total_count = count(coins, required_sum);
  cout<<"Total combinations are: "<<total_count<<endl;

}
