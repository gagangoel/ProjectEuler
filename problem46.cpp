#include <iostream>
#include "utils.h"

using namespace std;

int main() {

  vector<long> primes;
  primes.push_back(2);
  long num = 3L;
  bool found = false;
  long result;
  while(!found) {

    if(is_prime(num))
      primes.push_back(num);

    else {
      long i=0L;
      for(; (i<primes.size()) && (primes[i]<num); i++) {
        long temp = num - primes[i];
        // Check if the number is a perfect square
        if((temp%2 != 0) || ((temp/2)!=(floor(sqrt((temp/2)))*floor(sqrt((temp/2)))))) {
            continue;
          }
        else
           break;
        }
        if((i>=primes.size()) || (primes[i]>=num)) {
          cout<<"Found the number!"<<endl;
          result = num;
          found = true;
          break;
        }
      }
    num += 2;
  }
  cout<<"Smallest odd composite number at which Goldbach's conjecture does not hold is: "<<result<<endl;
}
