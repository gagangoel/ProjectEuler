class Problem3 {
	public static void main(String args[]) {
//		long num = 600851475143L;
//		long num = 13195L;
		long num = 11L;
		long num_orig = num;
		System.out.println("Prime factors are: ");
		boolean isPrime = true;
		for(long i=3L; i<=num; i++) {
			isPrime = true;
			for(long j=2L; j<i; j++) {
				if((i%j)==0) {
					isPrime=false;
					break;
				}
			}
			if(isPrime && i!=num_orig) {
//				System.out.println(i);
				if((num%i)==0) {
					System.out.println(i);
					num = num/i;
					while((num%i)==0) {
						num=num/i;
						System.out.println(i);
					}
				}
			}
		}
		System.out.println("End!");
	}
}
