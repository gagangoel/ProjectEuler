#include <iostream>
#include <vector>
#include <cmath>
#include <unordered_map>

using namespace std;

int main() {

  unordered_map<long, bool> pentagonal_map;
  vector<long> pentagonal;

  long count = 1L;
  bool found = false;
  long result1=0L, result2=0L;
  while(!found) {
    long cur_val = count*(3*count - 1)/2;
    pentagonal.push_back(cur_val);
    // Also creating an unordered_map here for easy look ups
    pentagonal_map[cur_val] = true;
    // This is the key part of this code. Start from the closet number to the number just added, since our aim is to minimize the difference between the pentagonal numbers.
    for(long j = pentagonal.size()-2; j>=0; j--) {
      long to_find = abs(pentagonal[j] - cur_val);
      if(pentagonal_map[to_find]==true) {
        long temp = pentagonal[j] + cur_val;
        temp = 1 + (temp*24);
        // Check if temp is a perfect square
        if(temp == (floor(sqrt(temp))*floor(sqrt(temp)))) {
          long temp_index = 1 + sqrt(temp);
          // Check if temp_index forms a valid index in the series of pentagonal numbers
          if(temp_index%6 == 0) {
            result1 = pentagonal[j];
            result2 = cur_val;
            cout<<"j: "<<j<<", cur: "<<(count-1)<<endl;
            found = true;
            break;
          }
        }
      }
    }
    count++;
  }
  cout<<"Result is the absolute value of the difference of: "<<result1<<", "<<result2<<" = "<<abs(result1-result2)<<endl;
}
